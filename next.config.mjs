/* eslint-disable @nlib/no-globals */
import EsifyCSSWebpackPlugin from 'esifycss-webpack-plugin';

const nextConfig = {
    reactStrictMode: true,
    webpack: (config) => {
        config.resolve.plugins.push(new EsifyCSSWebpackPlugin());
        return config;
    },
};

export default nextConfig;
