# co-editor

https://co-editor-gjbkz.vercel.app/

![構成図](./images/co-editor.jpg)

## ローカルで動かす場合

1. Auth0のアプリケーションを準備してください
    1. Application TypeはRegular Web Applicationにします
    1. 設定は https://github.com/auth0/nextjs-auth0#getting-started の通りにやればローカルで動かす分には十分です
    1. Vercel上で動かすにはVercelのドメインを設定に追加します

1. DynamoDBのテーブルを用意してください
    1. パーティションキーはPK（文字列）, ソートキーはSK（文字列）です
    1. パーティションキーとソートキーを入れ替えたSKPKというGSIを作成してください（まだ使う場面はない）
1. `.env.local`を準備してください。

```
APP_BASE_URL='localhost:3000'
AUTH0_SECRET='HEX64文字くらいの適当な文字列'
AUTH0_ISSUER_BASE_URL='https://あなたの.jp.auth0.com'
AUTH0_CLIENT_ID='Auth0の画面からコピーしてください'
AUTH0_CLIENT_SECRET='Auth0の画面からコピーしてください'
APP_AWS_ACCESS_KEY_ID='AWSコンソールで作成してください'
APP_AWS_SECRET_ACCESS_KEY='AWSコンソールで作成してください'
APP_AWS_REGION='ap-northeast-1'
APP_AWS_TABLE_NAME='co-editor'
```

準備ができたら `npm run dev` を実行すれば動きます。
