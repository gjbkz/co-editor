import {useRouter} from 'next/router';
import {isTeamId} from '../model/id';
import {isTeamProps} from '../model/Team';
import {endpoints} from '../util/endpoints';
import {useGetAPI} from './GetAPI';

export const useCurrentTeam = () => {
    const {query: {teamId}} = useRouter();
    const {data: team} = useGetAPI(isTeamId(teamId) ? endpoints.getTeam({teamId}) : null, isTeamProps);
    return team;
};
