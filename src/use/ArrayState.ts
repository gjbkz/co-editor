import type {Reducer} from 'react';
import {useCallback, useReducer} from 'react';

type Action<T> =
| {type: 'push', item: T}
| {type: 'unshift', item: T};

const reducer = <T>(
    current: Array<T>,
    action: Action<T>,
): Array<T> => {
    switch (action.type) {
    case 'push':
        return [...current, action.item];
    case 'unshift':
        return [action.item, ...current];
    default:
        return current;
    }
};

export const useArrayState = <T>(initialData: Array<T> = []) => {
    const [data, dispatch] = useReducer(reducer as Reducer<Array<T>, Action<T>>, initialData);
    const push = useCallback((item: T) => dispatch({type: 'push', item}), []);
    const unshift = useCallback((item: T) => dispatch({type: 'unshift', item}), []);
    return {data, push, unshift};
};
