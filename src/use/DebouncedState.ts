import {useEffect, useState} from 'react';

export const useDebouncedState = <T>(
    value: T,
    delayMs: number,
) => {
    const [currentValue, setCurrentValue] = useState<T>(value);
    useEffect(() => {
        const timer = setTimeout(() => {
            setCurrentValue(value);
        }, delayMs);
        return () => {
            clearTimeout(timer);
        };
    }, [value, delayMs]);
    return currentValue;
};
