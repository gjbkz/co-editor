import {useCallback, useEffect, useState} from 'react';

export const useDebouncedCallback = <Args extends Array<unknown>>(
    callback: (...args: Args) => void,
    dependencies: Array<unknown>,
    delayMs: number,
) => {
    const [lastArgs, setLastArgs] = useState<Args>();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const fn = useCallback(callback, dependencies);
    useEffect(() => {
        const timer = setTimeout(() => {
            if (lastArgs) {
                fn(...lastArgs);
            }
        }, delayMs);
        return () => {
            clearTimeout(timer);
        };
    }, [fn, lastArgs, delayMs]);
    return useCallback((...args: Args) => {
        setLastArgs(args);
    }, []);
};
