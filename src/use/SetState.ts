import {useCallback, useReducer} from 'react';

const reducer = <T>(
    current: Set<T>,
    action: {type: 'add', item: T} | {type: 'clear'} | {type: 'delete', item: T},
) => {
    const cloned = new Set(current);
    switch (action.type) {
    case 'add':
        cloned.add(action.item);
        return cloned;
    case 'delete':
        cloned.delete(action.item);
        return cloned;
    case 'clear':
        cloned.clear();
        return cloned;
    default:
        return current;
    }
};

export const useSetState = <T>(initialData: Set<T> = new Set()) => {
    const [data, dispatch] = useReducer(reducer, initialData);
    const addItem = useCallback((item: T) => dispatch({type: 'add', item}), []);
    const deleteItem = useCallback((item: T) => dispatch({type: 'delete', item}), []);
    const clearAll = useCallback(() => dispatch({type: 'clear'}), []);
    return {data, addItem, deleteItem, clearAll};
};
