import {useMemo} from 'react';
const encoder = new TextEncoder();

export const useByteLength = (...args: Array<object | string | null | undefined>) => useMemo(
    () => {
        let sum = 0;
        for (const arg of args) {
            if (arg) {
                const data = JSON.stringify(arg);
                const view = encoder.encode(data);
                sum += view.byteLength;
            }
        }
        return sum;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    args,
);
