import type {Definition} from '@nlib/typing';
import {ensure} from '@nlib/typing';
import {useRouter} from 'next/router';

export const useRouteQuery = <T>(hasRequiredProps: Definition<T>): T | null => {
    const {isReady, query} = useRouter();
    return isReady ? ensure(query, hasRequiredProps) : null;
};
