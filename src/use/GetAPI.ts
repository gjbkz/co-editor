import type {Definition} from '@nlib/typing';
import {ensure} from '@nlib/typing';
import {useCallback} from 'react';
import useSWR, {useSWRConfig} from 'swr';
import type {APIEndpoint} from '../util/endpoints';

/** GETリクエストの結果を使うコンポーネントを書きやすくするやつ */
export const useGetAPI = <T>(
    endpoint: APIEndpoint | null,
    hasRequiredType: Definition<T>,
) => {
    const fetcher = useCallback(async ({url, method}: APIEndpoint) => {
        const response = await fetch(url, {method});
        return ensure(await response.json(), hasRequiredType);
    }, [hasRequiredType]);
    const resource = useSWR(endpoint, fetcher);
    const {mutate} = useSWRConfig();
    const refresh = useCallback(() => {
        if (endpoint) {
            mutate(endpoint).catch((error) => {
                // eslint-disable-next-line no-console
                console.error(error);
            });
        }
    }, [mutate, endpoint]);
    return {...resource, refresh};
};
