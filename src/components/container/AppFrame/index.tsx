import {useUser} from '@auth0/nextjs-auth0';
import Link from 'next/link';
import type {PropsWithChildren} from 'react';
import {useCurrentTeam} from '../../../use/CurrentTeam';
import {classnames} from '../../../util/classnames';
import {UserCard} from '../../model/user/UserCard';
import {PrimaryButton, SecondaryButton} from '../../ui/Button';
import {ErrorText} from '../../ui/ErrorText';
import {className} from './style.module.css';

interface AppFrameProps {}

export const AppFrame = ({children}: PropsWithChildren<AppFrameProps>) => {
    const team = useCurrentTeam();
    const {isLoading, user, error} = useUser();
    const loginRequired = !isLoading && !user;
    return <>
        <header className={classnames(className.header)}>
            <h1 className={classnames(className.heading)}>
                <Link href="/"><a className={classnames(className.title)}>{team ? '🏠' : 'Co Editor'}</a></Link>
                {team && <Link href={`/${team.teamId}`}><a className={classnames(className.title)}>{team.teamName}</a></Link>}
            </h1>
            <div className={classnames(className.spacer)}/>
            <UserCard user={user} loading={isLoading}/>
            <div className={classnames(className.delimiter)}/>
            {user && <SecondaryButton href="/api/auth/logout">ログアウト</SecondaryButton>}
            {loginRequired && <PrimaryButton href="/api/auth/login">ログイン</PrimaryButton>}
        </header>
        <main>
            {user && children}
            {error && <ErrorText error={error}/>}
        </main>
    </>;
};
