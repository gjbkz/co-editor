import type {AnchorHTMLAttributes, ButtonHTMLAttributes} from 'react';
import {classnames} from '../../../util/classnames';
import {className} from './style.module.css';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    href?: string,
}

const Button = (
    props: ButtonProps,
) => {
    if (props.href) {
        return <a {...(props as AnchorHTMLAttributes<HTMLAnchorElement>)} className={classnames(className.button, props.className)}/>;
    }
    return <button {...props} className={classnames(className.button, props.className)}/>;
};

export const PrimaryButton = (
    props: ButtonProps,
) => <Button {...props} className={classnames(className.primary, props.className)}/>;

export const SecondaryButton = (
    props: ButtonProps,
) => <Button {...props} className={classnames(className.secondary, props.className)}/>;
