interface ErrorTextProps {
    error: Error,
}

export const ErrorText = ({error}: ErrorTextProps) => <span>
    {[error.message, error.stack].filter((message) => message).join('\n')}
</span>;
