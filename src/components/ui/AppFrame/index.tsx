import type {PropsWithChildren} from 'react';
import {classnames} from '../../../util/classnames';
import {className} from './style.module.css';

export const AppFrame = ({children}: PropsWithChildren<unknown>) => <>
    <header className={classnames(className.container)}>
        {children}
    </header>
</>;
