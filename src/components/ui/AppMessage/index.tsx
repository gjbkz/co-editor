import {useEffect, useState} from 'react';
import {classnames} from '../../../util/classnames';
import {className} from './style.module.css';

interface AppMessageProps {
    message?: string | null,
}

export const AppMessage = ({message}: AppMessageProps) => {
    const [current, setCurrent] = useState<AppMessageProps['message']>('');
    const [active, setActive] = useState(false);
    useEffect(() => {
        if (message && current !== message) {
            // eslint-disable-next-line no-console
            console.info(message);
            setCurrent(message);
            setActive(true);
        }
    }, [message, current]);
    useEffect(() => {
        const timerId = setTimeout(() => {
            if (active) {
                setActive(false);
            }
        }, 4000);
        return () => {
            clearTimeout(timerId);
        };
    }, [active]);
    return <div className={classnames(className.container)}>
        <div className={classnames(className.message, !active && className.hidden)}>
            {current}
        </div>
    </div>;
};
