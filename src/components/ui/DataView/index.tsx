import {classnames} from '../../../util/classnames';
import {className} from './style.module.css';

export const DataView = (
    {data, nested}: {
        data: object,
        nested?: boolean,
    },
) => {
    return <table className={classnames(className.table, nested && className.nested)}>
        <tbody>
            {Object.entries(data).map(([key, value]) => {
                return <tr key={key}>
                    <th className={classnames(className.th)}>{key}</th>
                    <td className={classnames(className.td)} data-type={getType(value)}><Value value={value as unknown}/></td>
                </tr>;
            })}
        </tbody>
    </table>;
};

const getType = (value: unknown) => {
    if (value === null) {
        return 'null';
    }
    const type = typeof value;
    if (type === 'function') {
        return 'object';
    }
    return type;
};

const Value = ({value}: {value: unknown}) => {
    switch (typeof value) {
    case 'object':
    case 'function':
        if (value) {
            return <DataView data={value as Record<string, unknown>} nested={true}/>;
        }
        break;
    case 'number':
        if (isTimeStampLike(value)) {
            return <>
                {JSON.stringify(value)}
                <div className={classnames(className.note)}>{new Date(value).toISOString()}</div>
            </>;
        }
        break;
    case 'undefined':
        return <div className={classnames(className.note)}>undefined</div>;
    default:
    }
    return <>{JSON.stringify(value)}</>;
};

const timeStampRange = 1000 * 60 * 60 * 24 * 365 * 50;
const minTimeStamp = Date.now() - timeStampRange;
const maxTimeStamp = minTimeStamp + timeStampRange;
const isTimeStampLike = (value: number) => minTimeStamp < value && value < maxTimeStamp;
