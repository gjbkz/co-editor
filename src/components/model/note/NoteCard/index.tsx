import Link from 'next/link';
import type {NoteProps} from '../../../../model/Note';
import {useByteLength} from '../../../../use/ByteLength';
import {classnames} from '../../../../util/classnames';
import {DataView} from '../../../ui/DataView';
import {className} from './style.module.css';

interface NoteCardProps {
    note: NoteProps,
}

export const NoteCard = ({note}: NoteCardProps) => {
    const byteLength = useByteLength(note);
    return <div className={classnames(className.container)}>
        <Link href={`/${note.teamId}/notes/${note.noteId}`}><a>{note.noteTitle || 'タイトルなし'}（クリックで編集開始）</a></Link>
        <DataView data={{size: `${(byteLength / 1000).toFixed(1)} KB`, ...note, body: '省略'}}/>
    </div>;
};
