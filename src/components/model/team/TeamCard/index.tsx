import Link from 'next/link';
import type {TeamProps} from '../../../../model/Team';
import {classnames} from '../../../../util/classnames';
import {DataView} from '../../../ui/DataView';
import {className} from './style.module.css';

interface TeamCardProps {
    team: TeamProps,
}

export const TeamCard = ({team}: TeamCardProps) => {
    return <div className={classnames(className.container)}>
        <Link href={`/${team.teamId}`}><a>{team.teamName}（クリックで開く）</a></Link>
        <DataView data={team}/>
    </div>;
};
