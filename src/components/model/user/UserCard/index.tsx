import type {UserProfile} from '@auth0/nextjs-auth0';
import {useCallback} from 'react';
import {classnames} from '../../../../util/classnames';
import {UserAvatar} from '../UserAvatar';
import {className} from './style.module.css';

export const UserCard = (
    {user, loading}: {
        user?: Pick<UserProfile, 'name' | 'nickname' | 'picture'>,
        loading?: boolean,
    },
) => {
    const userName = loading ? 'Loading...' : user && (user.nickname || user.name || '');
    const onClick = useCallback(() => {
        const message = [
            `${userName}としてログインしています。`,
            JSON.stringify(user, null, 2),
        ].join('\n');
        // eslint-disable-next-line no-alert
        alert(message);
    }, [user, userName]);
    return <div
        className={classnames(className.container)}
        title={userName}
        onClick={onClick}
    >
        <UserAvatar user={user}/>
        <div className={classnames(className.spacer)}/>
        <span className={classnames(className.name)}>{userName}</span>
    </div>;
};
