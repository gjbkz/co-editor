import {classnames} from '../../../../util/classnames';
import {className} from './style.module.css';
import type {UserProfile} from '@auth0/nextjs-auth0';

export const UserAvatar = (
    {user}: {
        user?: Pick<UserProfile, 'name' | 'nickname' | 'picture'>,
    },
) => <div className={classnames(className.container)}>
    {user && user.picture && <img src={`${user.picture}`} alt=""/>}
</div>;
