import {ensure} from '@nlib/typing';
import {useRouter} from 'next/router';
import {useCallback, useState} from 'react';
import {isTeamId} from '../../../model/id';
import {isNoteProps} from '../../../model/Note';
import {useCurrentTeam} from '../../../use/CurrentTeam';
import {useGetAPI} from '../../../use/GetAPI';
import {useRouteQuery} from '../../../use/RouteQuery';
import {classnames} from '../../../util/classnames';
import {endpoints} from '../../../util/endpoints';
import {AppFrame} from '../../container/AppFrame';
import {NoteCard} from '../../model/note/NoteCard';
import {PrimaryButton} from '../../ui/Button';
import {className} from './style.module.css';

export const TeamPage = () => {
    const router = useRouter();
    const team = useCurrentTeam();
    const query = useRouteQuery({teamId: isTeamId});
    const {data: notes} = useGetAPI(endpoints.listNotes(query), isNoteProps.array);
    const [loadingNewNote, setLoadingNewNote] = useState(false);
    const addNote = useCallback(() => {
        const endpoint = endpoints.createNote(team);
        if (!endpoint || !team) {
            return;
        }
        setLoadingNewNote(true);
        fetch(endpoint.url, {method: endpoint.method})
        .then(async (response) => {
            const note = ensure(await response.json(), isNoteProps);
            await router.push(`/${team.teamId}/notes/${note.noteId}`);
        })
        .catch((error) => {
            // eslint-disable-next-line no-alert
            alert(error);
            setLoadingNewNote(false);
        });
    }, [team, router]);
    return <AppFrame>
        <section className={classnames(className.buttons)}>
            <PrimaryButton onClick={addNote} disabled={loadingNewNote}>
                {loadingNewNote ? '作成中...' : '新しいノート'}
            </PrimaryButton>
        </section>
        <section className={classnames(className.list)}>
            {notes?.map((note) => <NoteCard key={note.noteId} note={note}/>)}
            {notes && notes.length === 0 && <div>上のボタンからノートを作成しましょう。</div>}
        </section>
    </AppFrame>;
};
