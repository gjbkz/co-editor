import {ensure} from '@nlib/typing';
import type {ChangeEvent, KeyboardEvent} from 'react';
import {useCallback, useEffect, useState} from 'react';
import type {BaseEditor, Descendant} from 'slate';
import {createEditor, Text, Transforms} from 'slate';
import type {ReactEditor, RenderElementProps, RenderLeafProps} from 'slate-react';
import {Editable, Slate, withReact} from 'slate-react';
import {isNoteId, isTeamId} from '../../../model/id';
import {isNoteProps} from '../../../model/Note';
import {useByteLength} from '../../../use/ByteLength';
import {useDebouncedCallback} from '../../../use/DebouncedCallback';
import {useDebouncedState} from '../../../use/DebouncedState';
import {useGetAPI} from '../../../use/GetAPI';
import {useRouteQuery} from '../../../use/RouteQuery';
import {classnames} from '../../../util/classnames';
import {endpoints} from '../../../util/endpoints';
import type {CustomElement, CustomText} from '../../../util/isCustomElement';
import {isSameCustomElement, isSameCustomText} from '../../../util/isCustomElement';
import {AppFrame} from '../../container/AppFrame';
import {AppMessage} from '../../ui/AppMessage';
import {DataView} from '../../ui/DataView';
import {className} from './style.module.css';

declare module 'slate' {
    interface CustomTypes {
        Editor: BaseEditor & ReactEditor,
        Element: CustomElement,
        Text: CustomText,
    }
}

const Rendered = ({attributes, element, children}: RenderElementProps) => {
    switch (element.type) {
    default:
        return <p {...attributes}>{children}</p>;
    }
};

const Leaf = ({attributes, children, leaf}: RenderLeafProps) => {
    return <span
        {...attributes}
        className={classnames(leaf.bold && className.bold)}
    >{children}</span>;
};

// eslint-disable-next-line max-lines-per-function
export const NotePage = () => {
    const query = useRouteQuery({teamId: isTeamId, noteId: isNoteId});
    const {data: note, refresh} = useGetAPI(endpoints.getNote(query), isNoteProps);
    const [message, setMessage] = useState('');
    const [value, setValue] = useState<Array<Descendant>>();
    const [lastSavedValue, setLastSavedValue] = useState(value);
    const debouncedValue = useDebouncedState(value, 600);
    const [editor] = useState(() => withReact(createEditor()));
    const onKeyDown = useCallback((event: KeyboardEvent<HTMLDivElement>) => {
        const special = event.ctrlKey || event.metaKey;
        if (special) {
            switch (event.key) {
            case 'b':
                event.preventDefault();
                Transforms.setNodes(editor, {bold: true}, {match: (n) => Text.isText(n), split: true});
                break;
            default:
            }
        }
    }, [editor]);
    const byteLength = useByteLength(note);
    useEffect(() => {
        if (note) {
            setValue(note.body);
        }
    }, [note]);
    useEffect(() => {
        if (!lastSavedValue && debouncedValue) {
            setLastSavedValue(debouncedValue);
        }
    }, [debouncedValue, lastSavedValue]);
    useEffect(() => {
        const abortController = new AbortController();
        if (debouncedValue && isChanged(lastSavedValue, debouncedValue)) {
            const endpoint = endpoints.patchNote(query);
            if (endpoint) {
                setMessage('本文を保存しています');
                fetch(endpoint.url, {
                    method: endpoint.method,
                    body: JSON.stringify({body: debouncedValue}),
                    signal: abortController.signal,
                })
                .then((response) => {
                    if (response.ok) {
                        setMessage('本文を保存しました');
                        setLastSavedValue(debouncedValue);
                        refresh();
                    }
                })
                .catch((error) => {
                    // eslint-disable-next-line no-console
                    console.error(error);
                });
            }
        }
        return () => abortController.abort();
    }, [debouncedValue, lastSavedValue, query, refresh]);
    return <AppFrame>
        <div className={classnames(className.editor)}>
            <div>
                <TitleEditor setMessage={setMessage} refresh={refresh}/>
                {value && <Slate editor={editor} value={value} onChange={setValue}>
                    <Editable renderElement={Rendered} renderLeaf={Leaf} onKeyDown={onKeyDown}/>
                </Slate>}
            </div>
            <div>
                <div className={classnames(className.size)}>
                    {(byteLength / 1000).toFixed(1)} KB
                </div>
                <ol>
                    {value && value.map((data, index) => <li key={index}><DataView data={data}/></li>)}
                </ol>
            </div>
        </div>
        <AppMessage message={message}/>
    </AppFrame>;
};

const TitleEditor = (
    {setMessage, refresh}: {
        setMessage: (message: string) => void,
        refresh: () => void,
    },
) => {
    const query = useRouteQuery({teamId: isTeamId, noteId: isNoteId});
    const {data: note} = useGetAPI(endpoints.getNote(query), isNoteProps);
    const [savedTitle, setSavedTitle] = useState('');
    const onChangeTitle = useDebouncedCallback((event: ChangeEvent<HTMLInputElement>) => {
        const abortController = new AbortController();
        const endpoint = endpoints.patchNote(query);
        if (endpoint) {
            const noteTitle = event.target.value;
            if (savedTitle !== noteTitle) {
                setMessage('タイトルを保存しています');
                fetch(endpoint.url, {
                    method: endpoint.method,
                    body: JSON.stringify({noteTitle}),
                })
                .then(async (response) => {
                    const newNote = ensure(await response.json(), isNoteProps);
                    setSavedTitle(newNote.noteTitle);
                    setMessage('タイトルを保存しました');
                    refresh();
                })
                .catch((error) => {
                    // eslint-disable-next-line no-console
                    console.error(error);
                });
            }
        }
        return () => abortController.abort();
    }, [query, savedTitle, refresh], 500);
    useEffect(() => {
        if (note) {
            setSavedTitle(note.noteTitle);
        }
    }, [note]);
    return <>
        {!note && <input className={classnames(className.title)} type="text" value="Loading..." readOnly/>}
        {note && <input
            className={classnames(className.title)}
            type="text"
            defaultValue={note.noteTitle}
            placeholder="ノートのタイトル"
            onChange={onChangeTitle}
        />}
    </>;
};

const isChanged = (
    listA?: Array<Descendant>,
    listB?: Array<Descendant>,
): boolean => {
    if (!(listA && listB)) {
        return false;
    }
    if (listA.length !== listB.length) {
        return true;
    }
    for (let index = listA.length; index--;) {
        const a = listA[index];
        const b = listB[index];
        if ('type' in a && 'type' in b) {
            if (!isSameCustomElement(a, b)) {
                return true;
            }
        } else if ('text' in a && 'text' in b) {
            if (!isSameCustomText(a, b)) {
                return true;
            }
        }
    }
    return false;
};
