import {useUser} from '@auth0/nextjs-auth0';
import {ensure} from '@nlib/typing';
import {useCallback, useMemo} from 'react';
import type {TeamId} from '../../../model/id';
import type {TeamProps} from '../../../model/Team';
import {isTeamProps} from '../../../model/Team';
import {useArrayState} from '../../../use/ArrayState';
import {useGetAPI} from '../../../use/GetAPI';
import {classnames} from '../../../util/classnames';
import {endpoints} from '../../../util/endpoints';
import {AppFrame} from '../../container/AppFrame';
import {TeamCard} from '../../model/team/TeamCard';
import {PrimaryButton} from '../../ui/Button';
import {className} from './style.module.css';

export const IndexPage = () => {
    const {user} = useUser();
    const {data: addedTeams, push: pushNewTeam} = useArrayState<TeamProps>();
    const {data: teamList} = useGetAPI(user ? endpoints.listTeams() : null, isTeamProps.array);
    const addTeam = useCallback(() => {
        // eslint-disable-next-line no-alert
        const teamName = prompt('新しいチーム名を入力してください', '新しいチーム')?.trim();
        if (teamName) {
            const body = JSON.stringify({teamName});
            const {method, url} = endpoints.createTeam();
            fetch(url, {method, body})
            .then(async (response) => {
                pushNewTeam(ensure(await response.json(), isTeamProps));
            })
            .catch(alert);
        }
    }, [pushNewTeam]);
    const teams = useMemo(() => {
        const result = new Map<TeamId, TeamProps>();
        for (const list of [addedTeams, teamList]) {
            if (list) {
                for (const team of list) {
                    result.set(team.teamId, team);
                }
            }
        }
        return [...result.values()];
    }, [addedTeams, teamList]);
    return <AppFrame>
        <section className={classnames(className.buttons)}>
            <PrimaryButton onClick={addTeam} disabled={!teamList}>
                チームを作る
            </PrimaryButton>
        </section>
        <section className={classnames(className.list)}>
            {teams.map((team) => <TeamCard key={team.teamId} team={team}/>)}
            {teams.length === 0 && <div>上のボタンからチームを作成してください。</div>}
        </section>
    </AppFrame>;
};
