import type {GuardedType} from '@nlib/typing';
import {createTypeChecker} from '@nlib/typing';
import {createIdGenerator} from '../util/createIdGenerator';

export type UserId = string & {_type: 'UserId'};
export const isUserId = createTypeChecker<UserId>(
    'UserId',
    (input: unknown): input is UserId => typeof input === 'string' && 8 < input.length,
);

export const {generate: generateTeamId, test: isTeamId} = createIdGenerator('Team');
export type TeamId = GuardedType<typeof isTeamId>;

export const {generate: generateMemberId, test: isMemberId} = createIdGenerator('Member');
export type MemberId = GuardedType<typeof isMemberId>;

export const {generate: generateNoteId, test: isNoteId} = createIdGenerator('Note');
export type NoteId = GuardedType<typeof isNoteId>;
