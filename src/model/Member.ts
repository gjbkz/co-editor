import {marshall} from '@aws-sdk/util-dynamodb';
import type {GuardedType} from '@nlib/typing';
import {createTypeChecker, isNonNegativeSafeInteger} from '@nlib/typing';
import {getMetaProps} from '../util/getMetaProps';
import {generateMemberId, isTeamId, isUserId} from './id';
import {Model} from './Model';

export const isMemberProps = createTypeChecker('MemberProps', {
    userId: isUserId,
    teamId: isTeamId,
    createdAt: isNonNegativeSafeInteger,
    updatedAt: isNonNegativeSafeInteger,
});

export type MemberProps = GuardedType<typeof isMemberProps>;

export class Member extends Model<MemberProps> {

    public static async createMember(
        partial: Partial<MemberProps> & Pick<MemberProps, 'teamId' | 'userId'>,
    ) {
        const member = new Member({...partial, ...getMetaProps(partial)});
        await member.save();
        return member;
    }

    public marshall() {
        return marshall({
            ...this.props,
            PK: generateMemberId(this.props.userId),
            SK: this.props.teamId,
        });
    }

}
