import type {AttributeValue} from '@aws-sdk/client-dynamodb';
import {PutItemCommand} from '@aws-sdk/client-dynamodb';
import {dynamodbClient, TableName} from '../util/dynamodb';
import {getMetaProps} from '../util/getMetaProps';
const encoder = new TextEncoder();

export abstract class Model<Props extends Record<string, unknown>> {

    private data: Readonly<Props>;

    public constructor(props: Props) {
        this.data = props;
    }

    public get props() {
        return {...this.data};
    }

    public get buffer() {
        const view = encoder.encode(JSON.stringify(this.props));
        return view.buffer;
    }

    public get byteLength() {
        return this.buffer.byteLength;
    }

    public toJSON() {
        return this.props;
    }

    /**
     * インスタンスのpropsを更新してDynamoDBに保存します。
     * immutableにしたいけどabstract classではできない。
     */
    public async save(patch?: Partial<Props>) {
        const {data} = this;
        this.data = {...data, ...patch, ...getMetaProps(data)};
        await dynamodbClient.send(new PutItemCommand({
            TableName,
            Item: this.marshall(),
        }));
        return this;
    }

    public abstract marshall(): Record<string, AttributeValue>;

}
