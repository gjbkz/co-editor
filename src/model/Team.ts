import {GetItemCommand, paginateQuery} from '@aws-sdk/client-dynamodb';
import {marshall} from '@aws-sdk/util-dynamodb';
import type {GuardedType} from '@nlib/typing';
import {createTypeChecker, ensure, isNonNegativeSafeInteger, isString} from '@nlib/typing';
import {APIError} from '../util/APIError';
import {attributesToObject} from '../util/attributesToObject';
import {dynamodbClient, TableName} from '../util/dynamodb';
import {getMetaProps} from '../util/getMetaProps';
import {HTTP_Forbidden, HTTP_NotFound} from '../util/httpStatusCode';
import type {NoteId, TeamId, UserId} from './id';
import {generateMemberId, generateNoteId, generateTeamId, isTeamId, isUserId} from './id';
import {isMemberProps, Member} from './Member';
import {Model} from './Model';
import {isNoteProps, Note} from './Note';

export const isTeamProps = createTypeChecker('TeamProps', {
    teamId: isTeamId,
    ownerId: isUserId,
    teamName: isString,
    teamMembers: isUserId.array,
    createdAt: isNonNegativeSafeInteger,
    updatedAt: isNonNegativeSafeInteger,
});

export type TeamProps = GuardedType<typeof isTeamProps>;

export class Team extends Model<TeamProps> {

    public static async findTeam(
        teamId: TeamId,
        userId: UserId,
    ): Promise<Team | null> {
        const key = {PK: generateTeamId.prefix, SK: teamId};
        const result = await dynamodbClient.send(new GetItemCommand({TableName, Key: marshall(key)}));
        if (!result.Item) {
            // eslint-disable-next-line no-console
            console.info('InvalidQuery', {...key, result});
            return null;
        }
        const parsed = attributesToObject(result.Item);
        if (!isTeamProps(parsed)) {
            return null;
        }
        const team = new this(parsed);
        team.checkUser(userId);
        return team;
    }

    public static async getTeam(
        teamId: TeamId,
        userId: UserId,
    ): Promise<Team> {
        const team = await this.findTeam(teamId, userId);
        if (!team) {
            throw new APIError({code: 'NotFound', statusCode: HTTP_NotFound});
        }
        return team;
    }

    public static async *listTeamMembers(userId: UserId): AsyncGenerator<Member> {
        const pagenator = paginateQuery({client: dynamodbClient, pageSize: 20}, {
            TableName,
            KeyConditionExpression: 'PK = :pk',
            ExpressionAttributeValues: marshall({
                ':pk': generateMemberId(userId),
            }),
        });
        for await (const {Items = []} of pagenator) {
            for (const item of Items) {
                yield new Member(ensure(attributesToObject(item), isMemberProps));
            }
        }
    }

    public static async *listTeams(userId: UserId): AsyncGenerator<Team> {
        for await (const member of this.listTeamMembers(userId)) {
            const team = await this.findTeam(member.props.teamId, userId);
            if (team) {
                yield team;
            }
        }
    }

    public static async createTeam(
        ownerId: UserId,
        partial: Partial<TeamProps> = {},
    ) {
        const team = new Team({
            teamId: generateTeamId(),
            ownerId,
            teamName: partial.teamName || '新しいチーム',
            teamMembers: [ownerId],
            ...getMetaProps(partial),
        });
        await team.save();
        const member = await team.addMember(ownerId);
        return {team, member};
    }

    public marshall() {
        return marshall({
            ...this.props,
            PK: generateTeamId.prefix,
            SK: this.props.teamId,
        });
    }

    public async addMember(userId: UserId) {
        return await Member.createMember({
            userId,
            teamId: this.props.teamId,
        });
    }

    public checkUser(userId: UserId) {
        if (!this.props.teamMembers.includes(userId)) {
            throw new APIError({code: 'Not a member', statusCode: HTTP_Forbidden});
        }
    }

    public async *listNotes(): AsyncGenerator<Note> {
        const pagenator = paginateQuery({client: dynamodbClient, pageSize: 20}, {
            TableName,
            KeyConditionExpression: 'PK = :pk',
            ExpressionAttributeValues: marshall({
                ':pk': generateNoteId(this.props.teamId),
            }),
        });
        for await (const {Items = []} of pagenator) {
            for (const item of Items) {
                yield new Note(ensure(attributesToObject(item), isNoteProps));
            }
        }
    }

    public async createNote(createdBy: UserId): Promise<Note> {
        this.checkUser(createdBy);
        return await Note.createNote(this.props.teamId, createdBy);
    }

    public async getNote(noteId: NoteId, userId: UserId): Promise<Note> {
        this.checkUser(userId);
        return await Note.getNote(this.props.teamId, noteId);
    }

}
