import {GetItemCommand} from '@aws-sdk/client-dynamodb';
import {marshall} from '@aws-sdk/util-dynamodb';
import type {GuardedType} from '@nlib/typing';
import {createTypeChecker, isNonNegativeSafeInteger, isString} from '@nlib/typing';
import {APIError} from '../util/APIError';
import {attributesToObject} from '../util/attributesToObject';
import {dynamodbClient, TableName} from '../util/dynamodb';
import {getMetaProps} from '../util/getMetaProps';
import {HTTP_NotFound} from '../util/httpStatusCode';
import type {CustomElement} from '../util/isCustomElement';
import {isCustomElement} from '../util/isCustomElement';
import type {NoteId, TeamId, UserId} from './id';
import {generateNoteId, isNoteId, isTeamId, isUserId} from './id';
import {Model} from './Model';

export const isNoteProps = createTypeChecker('NoteProps', {
    teamId: isTeamId,
    noteId: isNoteId,
    createdBy: isUserId,
    noteTitle: isString,
    createdAt: isNonNegativeSafeInteger,
    updatedAt: isNonNegativeSafeInteger,
    body: isCustomElement.array,
});

export type NoteProps = GuardedType<typeof isNoteProps>;
export class Note extends Model<NoteProps> {

    public static async findNote(teamId: TeamId, noteId: NoteId): Promise<Note | null> {
        const key = {PK: generateNoteId(teamId), SK: noteId};
        const result = await dynamodbClient.send(new GetItemCommand({TableName, Key: marshall(key)}));
        if (!result.Item) {
            // eslint-disable-next-line no-console
            console.info('InvalidQuery', {...key, result});
            return null;
        }
        const parsed = attributesToObject(result.Item);
        if (!isNoteProps(parsed)) {
            return null;
        }
        return new this(parsed);
    }

    public static async getNote(teamId: TeamId, noteId: NoteId): Promise<Note> {
        const note = await this.findNote(teamId, noteId);
        if (!note) {
            throw new APIError({code: 'NotFound', statusCode: HTTP_NotFound});
        }
        return note;
    }

    public static async createNote(
        teamId: TeamId,
        createdBy: UserId,
        partial: Partial<NoteProps> = {},
    ) {
        const noteId = generateNoteId();
        const note = new Note({
            teamId,
            noteId,
            createdBy,
            noteTitle: '',
            body: initialValue,
            ...getMetaProps(partial),
        });
        await note.save();
        return note;
    }

    public marshall() {
        return marshall({
            ...this.props,
            PK: generateNoteId(this.props.teamId),
            SK: this.props.noteId,
        });
    }

}

const initialValue: Array<CustomElement> = [
    {
        type: 'paragraph',
        children: [
            {text: 'ここのテキストは編集できます。範囲を選択してCtrl+Bまたは⌘+Bで太字になります。右半分はデータを見るためのものです。'},
        ],
    },
    {type: 'paragraph', children: [{text: ''}]},
    {
        type: 'paragraph',
        children: [
            {text: '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。弱虫やーい。と囃したからである。小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰を抜かす奴があるかと云ったから、この次は抜かさずに飛んで見せますと答えた。'},
        ],
    },
    {type: 'paragraph', children: [{text: ''}]},
    {
        type: 'paragraph',
        children: [
            {text: '親類のものから西洋製のナイフを貰って奇麗な刃を日に翳して、友達に見せていたら、一人が光る事は光るが切れそうもないと云った。切れぬ事があるか、何でも切ってみせると受け合った。そんなら君の指を切ってみろと注文したから、何だ指ぐらいこの通りだと右の手の親指の甲をはすに切り込んだ。幸ナイフが小さいのと、親指の骨が堅かったので、今だに親指は手に付いている。しかし創痕は死ぬまで消えぬ。'},
        ],
    },
    {type: 'paragraph', children: [{text: ''}]},
    {
        type: 'paragraph',
        children: [
            {text: '庭を東へ'},
            {text: '二十歩', bold: true},
            {text: 'に行き尽すと、南上がりにいささかばかりの'},
            {text: '菜園', bold: true},
            {text: 'があって、真中に'},
            {text: '栗の木が一本', bold: true},
            {text: '立っている。これは命より大事な栗だ。実の熟する時分は起き抜けに背戸を出て落ちた奴を拾ってきて、学校で食う。'},
            {text: '菜園の西側が'},
            {text: '山城屋', bold: true},
            {text: 'という質屋の庭続きで、この質屋に勘太郎という十三四の倅が居た。勘太郎は無論弱虫である。弱虫の癖に四つ目垣を乗りこえて、栗を盗みにくる。ある日の夕方折戸の蔭に隠れて、とうとう勘太郎を捕まえてやった。その時勘太郎は逃げ路を失って、一生懸命に飛びかかってきた。向うは二つばかり年上である。弱虫だが力は強い。鉢の開いた頭を、こっちの胸へ宛ててぐいぐい押した拍子に、勘太郎の頭がすべって、おれの袷の袖の中にはいった。邪魔になって手が使えぬから、無暗に手を振ったら、袖の中にある勘太郎の頭が、右左へぐらぐら靡いた。しまいに苦しがって袖の中から、おれの二の腕へ食い付いた。痛かったから勘太郎を垣根へ押しつけておいて、足搦をかけて向うへ倒してやった。山城屋の地面は菜園より六尺がた低い。勘太郎は四つ目垣を半分崩して、自分の領分へ真逆様に落ちて、ぐうと云った。勘太郎が落ちるときに、おれの袷の片袖がもげて、急に手が自由になった。その晩母が山城屋に詫びに行ったついでに袷の片袖も取り返して来た。'},
        ],
    },
];
