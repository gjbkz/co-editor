import {UserProvider} from '@auth0/nextjs-auth0';
import type {AppProps} from 'next/app';
import '../app.css';

export default function Root({Component, pageProps}: AppProps) {
    return <UserProvider>
        <Component {...pageProps}/>
    </UserProvider>;
}
