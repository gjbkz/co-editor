import {ensure, isString} from '@nlib/typing';
import {Team} from '../../../model/Team';
import {createAuthorizedHandler} from '../../../util/createAuthorizedHandler';
import {HTTP_OK} from '../../../util/httpStatusCode';

export default createAuthorizedHandler({
    /** Array<Team>を返します */
    GET: async (_req, res, session) => {
        const teamList: Array<Team> = [];
        for await (const teamId of Team.listTeams(session.userId)) {
            teamList.push(teamId);
        }
        res.status(HTTP_OK).json(teamList);
    },
    /** チームを作成します */
    POST: async (req, res, session) => {
        const data = ensure(
            JSON.parse(`${req.body}`),
            {teamName: isString},
        );
        const {team} = await Team.createTeam(session.userId, data);
        res.status(HTTP_OK).json(team);
    },
});
