import {ensure} from '@nlib/typing';
import {isTeamId} from '../../../../model/id';
import {Team} from '../../../../model/Team';
import {createAuthorizedHandler} from '../../../../util/createAuthorizedHandler';
import {HTTP_OK} from '../../../../util/httpStatusCode';

export default createAuthorizedHandler({
    /** チームの情報を返します */
    GET: async (req, res, session) => {
        const {teamId} = ensure(req.query, {teamId: isTeamId});
        const team = await Team.getTeam(teamId, session.userId);
        res.status(HTTP_OK).json(team);
    },
});
