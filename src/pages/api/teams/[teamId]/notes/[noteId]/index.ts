import {ensure, isString} from '@nlib/typing';
import {isNoteId, isTeamId} from '../../../../../../model/id';
import {Team} from '../../../../../../model/Team';
import {createAuthorizedHandler} from '../../../../../../util/createAuthorizedHandler';
import {HTTP_OK} from '../../../../../../util/httpStatusCode';
import {isCustomElement} from '../../../../../../util/isCustomElement';

export default createAuthorizedHandler({
    /** ノートの情報を返します */
    GET: async (req, res, session) => {
        const {teamId, noteId} = ensure(req.query, {teamId: isTeamId, noteId: isNoteId});
        const team = await Team.getTeam(teamId, session.userId);
        const note = await team.getNote(noteId, session.userId);
        res.status(HTTP_OK).json(note);
    },
    /** ノートの情報を更新します */
    PATCH: async (req, res, session) => {
        const {teamId, noteId} = ensure(req.query, {
            teamId: isTeamId,
            noteId: isNoteId,
        });
        const patch = ensure(JSON.parse(`${req.body}`), {
            noteTitle: isString.optional,
            body: isCustomElement.array.optional,
        });
        const team = await Team.getTeam(teamId, session.userId);
        const note = await team.getNote(noteId, session.userId);
        await note.save(patch);
        res.status(HTTP_OK).json(note);
    },
});
