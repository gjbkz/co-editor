import {ensure} from '@nlib/typing';
import {isTeamId} from '../../../../../model/id';
import {Team} from '../../../../../model/Team';
import {createAuthorizedHandler} from '../../../../../util/createAuthorizedHandler';
import {HTTP_OK} from '../../../../../util/httpStatusCode';

export default createAuthorizedHandler({
    /** TODO: ページネーションの実装 */
    GET: async (req, res, session) => {
        const {teamId} = ensure(req.query, {teamId: isTeamId});
        const team = await Team.getTeam(teamId, session.userId);
        const notes = [];
        for await (const note of team.listNotes()) {
            notes.push(note);
        }
        res.status(HTTP_OK).json(notes);
    },
    /** ノートを作る */
    POST: async (req, res, session) => {
        const {teamId} = ensure(req.query, {teamId: isTeamId});
        const team = await Team.getTeam(teamId, session.userId);
        const note = await team.createNote(session.userId);
        res.status(HTTP_OK).json(note);
    },
});
