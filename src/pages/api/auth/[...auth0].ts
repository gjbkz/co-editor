import {auth0Client} from '../../../util/auth0';

export default auth0Client.handleAuth();
