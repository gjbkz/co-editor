import {NextResponse} from 'next/server';
import type {NextRequest} from 'next/server';

export const middleware = (req: NextRequest) => {
    const {pathname} = req.nextUrl;
    switch (pathname) {
    case '/sitemap.xml':
        return respondSitemap();
    default:
        return undefined;
    }
};

const respondSitemap = () => new NextResponse(
    'Hello',
    {
        headers: {
            'content-type': 'application/xml; charset=utf-8',
            'cache-control': 'public, max-age=3600',
        },
    },
);
