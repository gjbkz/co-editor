import * as crypto from 'crypto';
import {createTypeChecker, isString} from '@nlib/typing';

const baseTime = new Date('2022-01-01T09:00Z').getTime();
const getRandomId = () => [
    (Date.now() - baseTime).toString(36).padStart(8, '0'),
    crypto.randomBytes(12).toString('base64url'),
].join('');

export const createIdGenerator = <T extends string>(
    prefix: T,
) => {
    const generate = Object.assign(
        (id = getRandomId()): `${T}_${string}` => `${prefix}_${id}`,
        {prefix},
    );
    const test = createTypeChecker<`${T}_${string}`>(
        `${prefix}Id`,
        (input: unknown): input is `${T}_${string}` => isString(input) && input.startsWith(`${prefix}_`),
    );
    return {generate, test};
};
