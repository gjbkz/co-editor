import {DynamoDBClient} from '@aws-sdk/client-dynamodb';

export const TableName = process.env.APP_AWS_TABLE_NAME as string;

export const dynamodbClient = new DynamoDBClient({
    region: process.env.APP_AWS_REGION as string,
    credentials: {
        secretAccessKey: process.env.APP_AWS_SECRET_ACCESS_KEY as string,
        accessKeyId: process.env.APP_AWS_ACCESS_KEY_ID as string,
    },
});
