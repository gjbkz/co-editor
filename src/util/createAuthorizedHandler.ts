import type {Session} from '@auth0/nextjs-auth0';
import type {NextApiHandler, NextApiRequest, NextApiResponse} from 'next';
import type {UserId} from '../model/id';
import {isUserId} from '../model/id';
import {isAPIError} from './APIError';
import {auth0Client} from './auth0';
import type {HttpMethod} from './httpMethod';
import {HTTP_InternalServerError, HTTP_NotFound, HTTP_Unauthorized} from './httpStatusCode';

interface AuthorizedApiHandler {
    (
        req: NextApiRequest,
        res: NextApiResponse,
        session: Session & {userId: UserId},
    ): (Promise<void> | void),
}

export const createAuthorizedHandler = (
    handlers: Partial<Record<HttpMethod, AuthorizedApiHandler>>,
): NextApiHandler => async (req, res) => {
    const session = auth0Client.getSession(req, res);
    if (!session) {
        res.status(HTTP_Unauthorized).end();
        return;
    }
    const {sub: userId} = session.user;
    if (!isUserId(userId)) {
        res.status(HTTP_Unauthorized).end();
        return;
    }
    // eslint-disable-next-line no-console
    console.info(`${req.method} ${req.url} (${userId})`);
    const handler = handlers[req.method as HttpMethod];
    if (!handler) {
        res.status(HTTP_NotFound).end();
        return;
    }
    try {
        await handler(req, res, {...session, userId});
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
        if (isAPIError(error)) {
            res.status(error.statusCode).json({
                code: error.code,
                message: error.message,
            });
        } else {
            res.status(HTTP_InternalServerError).end();
        }
    }
};

