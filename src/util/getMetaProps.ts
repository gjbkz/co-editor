export interface MetaProps {
    createdAt: number,
    updatedAt: number,
}

export const getMetaProps = (partial: Partial<MetaProps> = {}) => {
    const updatedAt = Date.now();
    return {
        createdAt: partial.createdAt || updatedAt,
        updatedAt,
    };
};
