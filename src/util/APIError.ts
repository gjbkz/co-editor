import {createTypeChecker, isPositiveSafeInteger, isString} from '@nlib/typing';

export class APIError extends Error {

    public readonly code: string;

    public readonly statusCode: number;

    public constructor(
        {code, statusCode, message}: {
            code: string,
            statusCode: number,
            message?: string,
        },
    ) {
        super(message ? `${code}: ${message}` : code);
        this.code = code;
        this.statusCode = statusCode;
    }

}

export const isAPIError = createTypeChecker<APIError>('APIError', {
    name: isString,
    code: isString,
    statusCode: isPositiveSafeInteger,
    message: isString,
});
