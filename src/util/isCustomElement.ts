import {createTypeChecker, isBoolean, isString} from '@nlib/typing';

export interface CustomText {
    text: string,
    bold?: boolean,
}

export const isCustomText = createTypeChecker<CustomText>('CustomText', {
    text: isString,
    bold: isBoolean.optional,
});

export const isSameCustomText = (
    a: CustomText,
    b: CustomText,
): boolean => a.text === b.text && Boolean(a.bold) === Boolean(b.bold);

export interface CustomElement {
    type: 'paragraph',
    children: Array<CustomText>,
}

export const isCustomElement = createTypeChecker<CustomElement>('CustomElement', {
    type: /paragraph/,
    children: isCustomText.array,
});

export const isSameCustomElement = (
    a: CustomElement,
    b: CustomElement,
): boolean => {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (a.type !== b.type || a.children.length !== b.children.length) {
        return false;
    }
    return a.children.every((childA, index) => isSameCustomText(childA, b.children[index]));
};
