import type {NoteId, TeamId} from '../model/id';
import type {HttpMethod} from './httpMethod';

export interface APIEndpoint {
    method: HttpMethod,
    url: string,
}

const staticEndpoint = (
    method: HttpMethod,
    url: string,
) => (): APIEndpoint => ({method, url});

const endpoint = <T extends Record<string, unknown>>(
    method: HttpMethod,
    getter: (props: T) => string,
) => (props?: T | null): APIEndpoint | null => props ? ({method, url: getter(props)}) : null;

export const endpoints = {
    listTeams: staticEndpoint('GET', '/api/teams'),
    createTeam: staticEndpoint('POST', '/api/teams'),
    getTeam: endpoint('GET', (p: {teamId: TeamId}) => `/api/teams/${p.teamId}`),
    listNotes: endpoint('GET', (p: {teamId: TeamId}) => `/api/teams/${p.teamId}/notes`),
    createNote: endpoint('POST', (p: {teamId: TeamId}) => `/api/teams/${p.teamId}/notes`),
    getNote: endpoint('GET', (p: {teamId: TeamId, noteId: NoteId}) => `/api/teams/${p.teamId}/notes/${p.noteId}`),
    patchNote: endpoint('PATCH', (p: {teamId: TeamId, noteId: NoteId}) => `/api/teams/${p.teamId}/notes/${p.noteId}`),
};
