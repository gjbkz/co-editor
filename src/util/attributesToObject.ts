import type {AttributeValue} from '@aws-sdk/client-dynamodb';
import {unmarshall} from '@aws-sdk/util-dynamodb';

export const attributesToObject = (
    attributes: Record<string, AttributeValue>,
): Record<string, unknown> => {
    const result = unmarshall(attributes);
    delete result.PK;
    delete result.SK;
    return result;
};
