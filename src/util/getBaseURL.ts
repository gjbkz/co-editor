export const getBaseURL = (host: string | null | undefined) => {
    const appHost = host || 'localhost:3000';
    const protocol = (/^localhost:\d+$/).test(appHost) ? 'http' : 'https';
    return new URL(`${protocol}://${appHost}`);
};
