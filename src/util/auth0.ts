import {initAuth0} from '@auth0/nextjs-auth0';
import {getBaseURL} from './getBaseURL';

const baseURL = getBaseURL(process.env.APP_BASE_URL);

export const auth0Client = initAuth0({
    baseURL: baseURL.href,
});
